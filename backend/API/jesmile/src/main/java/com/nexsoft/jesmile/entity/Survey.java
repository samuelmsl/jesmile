package com.nexsoft.jesmile.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "survey")

public class Survey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 10)
    private int id_survey;

    @Column(nullable = false, length = 30)
    private String name_survey;
    @Column(nullable = false)
    private String message_survey;
    @Column(nullable = false)
    private String message_2_survey;
    @Column(nullable = false, length = 30)
    private String sales_survey;
    @Column(nullable = false)
    private int fk_survey;
    @Column
    private String cb_answer_1_survey;
    @Column
    private String cb_answer_2_survey;
}
