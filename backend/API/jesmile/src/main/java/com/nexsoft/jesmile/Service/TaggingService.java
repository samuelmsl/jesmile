package com.nexsoft.jesmile.Service;

import com.nexsoft.jesmile.Exception.FileStorageException;
import com.nexsoft.jesmile.entity.Tagging;
import com.nexsoft.jesmile.repository.TaggingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class TaggingService {
    @Autowired
    private TaggingRepository taggingRepository;

    public Tagging storeFile(MultipartFile file, String message_tagging,  String sales_tagging, String serial_tagging, int fk_tagging) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try{
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! File name contains invalid path sequence" + fileName);
            }

            Tagging tagging = new Tagging(fileName, file.getContentType(), file.getBytes(), message_tagging, serial_tagging, sales_tagging, fk_tagging);
            return taggingRepository.save(tagging);
        } catch (IOException e) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!.");
        }
    }
//    public Tagging getFile(int fileId) {return taggingRepository.findById(fileId).orElse(null);}
}

