package com.nexsoft.jesmile.controller;

import com.nexsoft.jesmile.dto.DTOResponse;
import com.nexsoft.jesmile.dto.SurveySalesResponse;
import com.nexsoft.jesmile.entity.Customer;
import com.nexsoft.jesmile.entity.Survey;
import com.nexsoft.jesmile.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.stream.events.DTD;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @PostMapping("/addCustomer")
    private Customer addCustomer(@RequestBody Customer customer) {return customerRepository.save(customer);}


    @GetMapping("/customer")
    private List<Customer> getAllCustomer() {return customerRepository.findAll();}

    @GetMapping("/customer/{id_customer}")
    private List<Customer> getCustomer(@PathVariable int id_customer) {return customerRepository.getAllCustomerById(id_customer);}

    @DeleteMapping("/deleteCustomer/{id}")
    private String deleteCustomer(@PathVariable int id) { customerRepository.deleteById(id); return "Data Berhasil dihapus!"; }

    @GetMapping("/customerSurvey/{sales_survey}")
    private List<Object> getAllCustomerSurvey(@PathVariable String sales_survey) {return customerRepository.getAllSurveyBySales(sales_survey);}

//    @GetMapping("/customerSurvey")
//    private List<DTOResponse> getAllSurvey() {return customerRepository.getAllSurvey();}
}
