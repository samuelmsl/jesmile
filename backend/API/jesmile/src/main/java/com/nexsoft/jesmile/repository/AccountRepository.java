package com.nexsoft.jesmile.repository;

import com.nexsoft.jesmile.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByUsername(String username);

//    @Query("select a from Account a where a.username collate latin1_general_cs like :username ")


    @Query(value = "SELECT * FROM account WHERE account.username COLLATE latin1_general_cs = :username", nativeQuery = true)
    Account findByUsernameQuery(String username);


    List<Account> findById(int id);

    @Query("select a from Account a where a.id =:id")
    public Object findPhoto(int id);

    @Query("select a from Account a where a.id = :id and a.token = :token")
    public List<Account> findByToken(int id, String token);

    @Query("select a from Account a where authority = :salestype")
    public List<Account> getSalesType(String salestype);

    @Transactional
    @Modifying
    @Query(value = "UPDATE account SET account.token = :token WHERE account.id = :id", nativeQuery = true)
    public int updateToken(@Param("id") int id, @Param("token") String token);




}
