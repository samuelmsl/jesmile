package com.nexsoft.jesmile.repository;
import com.nexsoft.jesmile.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Query(value = "SELECT customer.id_customer, customer.name_customer, customer.address_customer, customer.company_customer, survey.message_survey, survey.sales_survey, survey.fk_survey FROM customer JOIN survey on survey.name_survey = customer.name_customer WHERE survey.sales_survey =:sales_surs", nativeQuery = true)
    public List<Object> getAllSurveyBySales(String sales_surs);

    @Query(value = "SELECT * FROM customer c WHERE id_customer LIKE :id_customer", nativeQuery = true)
    public List<Customer> getAllCustomerById(int id_customer);
}
