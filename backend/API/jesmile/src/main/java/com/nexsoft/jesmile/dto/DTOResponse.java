package com.nexsoft.jesmile.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DTOResponse {

    private int id_customer;
    private String name_customer;
    private String address_customer;
    private String company_customer;

    private String message_survey;
    private String sales_survey;
    private String fk_survey;


    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getName_customer() {
        return name_customer;
    }

    public void setName_customer(String name_customer) {
        this.name_customer = name_customer;
    }

    public String getAddress_customer() {
        return address_customer;
    }

    public void setAddress_customer(String address_customer) {
        this.address_customer = address_customer;
    }

    public String getCompany_customer() {
        return company_customer;
    }

    public void setCompany_customer(String company_customer) {
        this.company_customer = company_customer;
    }

    public String getMessage_survey() {
        return message_survey;
    }

    public void setMessage_survey(String message_survey) {
        this.message_survey = message_survey;
    }

    //
    public String getSales_survey() {
        return sales_survey;
    }

    public void setSales_survey(String sales_survey) {
        this.sales_survey = sales_survey;
    }

    public String getFk_survey() {
        return fk_survey;
    }

    public void setFk_survey(String fk_survey) {
        this.fk_survey = fk_survey;
    }

}
