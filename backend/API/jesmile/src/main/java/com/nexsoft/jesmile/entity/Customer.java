package com.nexsoft.jesmile.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 10)
    private int id_customer;

    @Column(nullable = false, length = 30)
    private String name_customer;

    @Column(nullable = false, length = 100)
    private String address_customer;

    @Column(nullable = false, length = 30)
    private String company_customer;

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getName_customer() {
        return name_customer;
    }

    public void setName_customer(String name_customer) {
        this.name_customer = name_customer;
    }

    public String getAddress_customer() {
        return address_customer;
    }

    public void setAddress_customer(String address_customer) {
        this.address_customer = address_customer;
    }

    public String getCompany_customer() {
        return company_customer;
    }

    public void setCompany_customer(String company_customer) {
        this.company_customer = company_customer;
    }

    public List<Tagging> getTaggings() {
        return taggings;
    }

    public void setTaggings(List<Tagging> taggings) {
        this.taggings = taggings;
    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }

    @OneToMany(targetEntity = Tagging.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_tagging", referencedColumnName = "id_customer")
    private List<Tagging> taggings;


    @OneToMany(targetEntity = Survey.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_survey", referencedColumnName = "id_customer")
    private List<Survey> surveys;

}
