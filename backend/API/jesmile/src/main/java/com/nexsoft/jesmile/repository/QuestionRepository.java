package com.nexsoft.jesmile.repository;

import com.nexsoft.jesmile.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Integer> {

}
