package com.nexsoft.jesmile.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 10)
    private int id_question;

    @Column(nullable = false)
    private String input_question;

    @Column(nullable = false)
    private String radiobutton_question;

    @Column(nullable = false)
    private String radiobutton_answer1;

    @Column(nullable = false)
    private String radiobutton_answer2;

    @Column(nullable = false)
    private String radiobutton_answer3;

    @Column(nullable = false)
    private String radiobutton_answer4;

    @Column(nullable = false)
    private String checkbox_question;

    @Column(nullable = false)
    private String checkbox_answer1;

    @Column(nullable = false)
    private String checkbox_answer2;
}
