package com.nexsoft.jesmile.repository;

import com.nexsoft.jesmile.entity.ListSurvey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface ListSurveyRepository extends JpaRepository<ListSurvey, Integer> {

    @Query("select a from ListSurvey a where sales_list_survey = :salesname and date_list_survey = :now")
    public List<ListSurvey> getListSurveySales(String salesname, LocalDate now);

}
