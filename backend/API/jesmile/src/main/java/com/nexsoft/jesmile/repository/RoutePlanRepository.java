package com.nexsoft.jesmile.repository;

import com.nexsoft.jesmile.entity.RoutePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface RoutePlanRepository extends JpaRepository<RoutePlan, Integer> {
    @Query("select r from RoutePlan r where sales_route = :salesname and date_route like :now ")
    public List<RoutePlan> getRouteSales(String salesname, LocalDate now);
}
