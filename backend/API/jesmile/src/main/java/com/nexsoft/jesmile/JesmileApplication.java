package com.nexsoft.jesmile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JesmileApplication {

	public static void main(String[] args) {
		SpringApplication.run(JesmileApplication.class, args);
	}

}
