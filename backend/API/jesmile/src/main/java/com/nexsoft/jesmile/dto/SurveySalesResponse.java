package com.nexsoft.jesmile.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SurveySalesResponse {


    private int id_customer;
    private String name_customer;
    private String address_customer;
    private String company_customer;

    private String message_survey;
    private String sales_survey;
    private int fk_survey;

}
