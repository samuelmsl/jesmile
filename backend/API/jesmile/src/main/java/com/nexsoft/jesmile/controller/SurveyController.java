package com.nexsoft.jesmile.controller;

import com.nexsoft.jesmile.entity.ListSurvey;
import com.nexsoft.jesmile.entity.Question;
import com.nexsoft.jesmile.entity.Survey;
import com.nexsoft.jesmile.repository.QuestionRepository;
import com.nexsoft.jesmile.repository.SurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//import java.util.List

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SurveyController {

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private QuestionRepository questionRepository;



    @PostMapping("/addSurvey")
    private Survey addSurvey(@RequestBody Survey survey) {return surveyRepository.save(survey);}

    @PostMapping("/addAllSurvey")
    private List<Survey> addAllSurvey (@RequestBody List<Survey> surveys) {return surveyRepository.saveAll(surveys);}

    @GetMapping("/survey")
    private List<Survey> getAllSurvey() {return surveyRepository.findAll();}

    @GetMapping("/survey/{sales_survey}")
    private List<Survey> getSurveyBySales(@PathVariable String sales_survey)
    {return surveyRepository.getSurveyBySales(sales_survey);}

    @DeleteMapping("/deleteSurvey/{id_survey}")
    private String deleteById(@PathVariable int id_survey)
    {surveyRepository.deleteById(id_survey);
    return "survey berhasil dihapus!";}

    @GetMapping("/soalSurvey")
    private List<Question> getSoal() {
        return questionRepository.findAll();
    }

    @PostMapping("/addSoal")
    private Question postSoal(@RequestBody Question question) {
        return questionRepository.save(question);
    }


}

