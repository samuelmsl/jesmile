package com.nexsoft.jesmile.Util;

import com.nexsoft.jesmile.dto.LoginResponse;
import com.nexsoft.jesmile.entity.Account;
import com.nexsoft.jesmile.repository.AccountRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.internal.Function;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class JWTUtil {
    private final String secretKey = "random_secret_key";
    private final int EXPIRATION_TIME = 1000 * 1000 * 60 * 2;

    @Autowired
    private AccountRepository accountRepository;

    public LoginResponse generateToken(String username) {
        Account account = accountRepository.findByUsernameQuery(username);
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", account.getUsername());
        claims.put("authority", account.getAuthority());
        claims.put("id", account.getId());
        claims.put("role", account.getRole());
        claims.put("password", account.getPassword());
        return createToken(claims, account.getRole(), account.getId(),account.getPassword(), account.getUsername(), account.getName(), account.getAuthority());
    }

    private LoginResponse createToken(Map<String, Object> claims, String role, int id, String password, String username, String name, String authority) {
        Calendar expr = Calendar.getInstance();
        expr.add(Calendar.SECOND, 300);
        Date now = new Date(System.currentTimeMillis());
        Date expDate = new Date(now.getTime() + EXPIRATION_TIME);

        String token = Jwts
                .builder()
                .setClaims(claims)
                .setId(authority)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expDate)
                .setAudience(String.valueOf(id))
                .setIssuer(String.valueOf(id))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();

        LoginResponse response = new LoginResponse();
        response.setToken(token);
        response.setStatus(200);
        response.setError(null);
        response.setMessage("Success");
        return response;
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}
