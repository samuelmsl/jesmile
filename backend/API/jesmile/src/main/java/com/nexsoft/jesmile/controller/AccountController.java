package com.nexsoft.jesmile.controller;

import com.nexsoft.jesmile.dto.LoginRequest;
import com.nexsoft.jesmile.dto.LoginResponse;
import com.nexsoft.jesmile.dto.TokenRequest;
import com.nexsoft.jesmile.entity.Account;
import com.nexsoft.jesmile.repository.AccountRepository;
import com.nexsoft.jesmile.Util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AccountController {
    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AccountRepository accountRepository;

    @PostMapping("/public/register")
    public Account register(@RequestBody Account account) {
        return accountRepository.save(account);
    }

    @PutMapping("/updateToken/{id}/{token}")
    public int updateToken(@PathVariable("id") int id, @PathVariable("token") String token) {
        return accountRepository.updateToken(id, token);
    }

    @GetMapping("/salesType/{authority}")
    public List<Account> getSalesType(@PathVariable String authority) {
        return accountRepository.getSalesType(authority);
    }

    @PostMapping("/auth")
    public LoginResponse authenticate(@RequestBody LoginRequest account) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(account.getUsername(), account.getPassword())
            );
        } catch (Exception e) {
            e.printStackTrace();
            LoginResponse failedResponse = new LoginResponse();
            failedResponse.setMessage("Invalid username or password!!");
            failedResponse.setStatus(403);
            failedResponse.setError("Invalid");
            return failedResponse;
        }
        return jwtUtil.generateToken(account.getUsername());
    }


    @GetMapping("/account")
    public List<Account> getAllAccount() {
        return accountRepository.findAll();
    }

    @GetMapping("/accountById/{id}")
    public List<Account> getById(@PathVariable int id) {
        return accountRepository.findById(id);
    }

    @GetMapping("/salesFoto/{id}")
    public Object getFoto(@PathVariable int id) {
        return accountRepository.findPhoto(id);
    }

    @GetMapping("/accountByToken/{id}/{token}")
    public List<Account> getById(@PathVariable("id") int id, @PathVariable("token") String token) {
        return accountRepository.findByToken(id, token);
    }

}
