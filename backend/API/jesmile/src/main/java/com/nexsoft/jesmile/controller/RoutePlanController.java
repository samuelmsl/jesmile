package com.nexsoft.jesmile.controller;

//import com.nexsoft.jesmile.dto.RoutePlanResponse;
import com.nexsoft.jesmile.entity.RoutePlan;
import com.nexsoft.jesmile.repository.RoutePlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.RouteMatcher;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RoutePlanController {
    @Autowired
    private RoutePlanRepository routePlanRepository;

    @GetMapping("/routePlan")
    private List<RoutePlan> getRoute() {return routePlanRepository.findAll();}

    @GetMapping("/routePlan/{salesname}")
    private List<RoutePlan> getRouteSales(@PathVariable String salesname, LocalDate localDate) {return routePlanRepository.getRouteSales(salesname, LocalDate.now());}

    @PostMapping("/addRoutePlan")
    private RoutePlan addRoute(@RequestBody RoutePlan routePlan) {return routePlanRepository.save(routePlan);}

    @DeleteMapping("/deleteRoutePlan/{id}")
    private String deleteRoutePlan(@PathVariable int id) { routePlanRepository.deleteById(id); return "Data Berhasil dihapus!"; }

}
