package com.nexsoft.jesmile.controller;

//import com.nexsoft.jesmile.Service.TaggingService;
//import com.nexsoft.jesmile.dto.TaggingSalesResponse;
import com.nexsoft.jesmile.Service.TaggingService;
import com.nexsoft.jesmile.dto.TaggingSalesResponse;
import com.nexsoft.jesmile.entity.Tagging;
import com.nexsoft.jesmile.repository.TaggingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//import javax.swing.text.html.HTML;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Component
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TaggingController {
    @Autowired
    private TaggingRepository taggingRepository;

    @Autowired
    private TaggingService taggingService;

    @PostMapping("/uploadAllTagging")
    private List<Tagging> uploadAllTag(@RequestBody List<Tagging> taggings ) { return taggingRepository.saveAll(taggings);}

//    @PostMapping("/addTagging")
//    private Tagging addTagging(@RequestBody Tagging tagging) {return taggingRepository.save(tagging);}

    @GetMapping("/tagging")
    private List<Tagging> getAllTagging() {return taggingRepository.findAll();}

    @GetMapping("/tagging/{sales_tagging}")
    private List<Tagging> getTaggingBySales(@PathVariable String sales_tagging) {return taggingRepository.getTaggingBySales(sales_tagging);}

    @DeleteMapping("/deleteTagging/{id_tagging}")
    private String deleteById(@PathVariable int id_tagging)
    {taggingRepository.deleteById(id_tagging);
        return "tagging berhasil dihapus!";
    }

    @PostMapping(value = "/uploadFile",  consumes = { MediaType.MULTIPART_FORM_DATA_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public TaggingSalesResponse uploadFile(@RequestPart("files") MultipartFile file, @RequestPart("message") String message_tagging,
                                           @RequestPart("sales") String sales_tagging, @RequestPart("serial") String serial_tagging, @RequestPart("fk_tagging") String fk_tagging) {

        int inum = Integer.parseInt(fk_tagging);
        Tagging fileName = taggingService.storeFile(file, message_tagging, sales_tagging, serial_tagging, inum);

        String fileDownloadUri  = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName.getFileName())
                .toUriString();
        return new TaggingSalesResponse(fileName.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
    }

//    @GetMapping("/downloadFile/{fileName:.+}")
//    public ResponseEntity<Resource> downloadFile(@PathVariable Long fileName, HttpServletRequest request) {
//        Tagging tagging = TaggingService.getFile(fileName);
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(tagging.getFileType()))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + tagging.getFileName() + "\"")
//                .body(new ByteArrayResource(tagging.getData()));
//    }


}
