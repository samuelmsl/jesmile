package com.nexsoft.jesmile.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ListSurvey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 10)
    private int id_list_survey;

    @Column
    private String customer_list_survey;

    @Column
    private String sales_list_survey;

    @Column
    private LocalDate date_list_survey;

    @Column
    private int fk_list_survey;

    public int getFk_list_survey() {
        return fk_list_survey;
    }

    public void setFk_list_survey(int fk_list_survey) {
        this.fk_list_survey = fk_list_survey;
    }

    public LocalDate getDate_list_survey() {
        return date_list_survey;
    }

    public void setDate_list_survey(LocalDate date_list_survey) {
        this.date_list_survey = date_list_survey;
    }

    @PrePersist
    protected void onCreate() {
        this.date_list_survey = LocalDate.now();
    }


    public int getId_list_survey() {
        return id_list_survey;
    }

    public void setId_list_survey(int id_list_survey) {
        this.id_list_survey = id_list_survey;
    }

    public String getCustomer_list_survey() {
        return customer_list_survey;
    }

    public void setCustomer_list_survey(String customer_list_survey) {
        this.customer_list_survey = customer_list_survey;
    }

    public String getSales_list_survey() {
        return sales_list_survey;
    }

    public void setSales_list_survey(String sales_list_survey) {
        this.sales_list_survey = sales_list_survey;
    }
}
