package com.nexsoft.jesmile.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
//@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "routeplan")
public class RoutePlan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int id_route;
    @Column(nullable = false)
    private String customer_route;
    @Column(nullable = false)
    private String sales_route;

    @Column
    private LocalDate date_route;

    @Column(nullable = false)
    private int fk_route;

    public int getFk_route() {
        return fk_route;
    }

    public void setFk_route(int fk_route) {
        this.fk_route = fk_route;
    }

    @PrePersist
    protected void onCreate(){
        this.date_route = LocalDate.now();
    }

    public RoutePlan(int id_route, String customer_route, String sales_route) {
        this.id_route = id_route;
        this.customer_route = customer_route;
        this.sales_route = sales_route;
    }

    public int getId_route() {
        return id_route;
    }

    public void setId_route(int id_route) {
        this.id_route = id_route;
    }

    public String getCustomer_route() {
        return customer_route;
    }

    public void setCustomer_route(String customer_route) {
        this.customer_route = customer_route;
    }

    public String getSales_route() {
        return sales_route;
    }

    public void setSales_route(String sales_route) {
        this.sales_route = sales_route;
    }
}
