package com.nexsoft.jesmile.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tagging")
public class Tagging {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 10)
    private int id_tagging;

    @Column
    private String fileName;
    @Column
    private String fileType;
    @Lob
    private byte[] data;
    @Column(nullable = false)
    private String message_tagging;
    @Column(nullable = false, length = 10)
    private String serial_tagging;
    @Column(length = 30)
    private String sales_tagging;
    @Column(length = 8)
    private int fk_tagging;

    public Tagging(String fileName, String fileType, byte[] data, String message_tagging, String serial_tagging, String sales_tagging, Integer fk_tagging) {
//        this.id_tagging = id_tagging;
        this.fileName = fileName;
        this.fileType = fileType;
        this.data = data;
        this.message_tagging = message_tagging;
        this.serial_tagging = serial_tagging;
        this.sales_tagging = sales_tagging;
        this.fk_tagging = fk_tagging;
    }

    public int getId_tagging() {
        return id_tagging;
    }

    public void setId_tagging(int id_tagging) {
        this.id_tagging = id_tagging;
    }

    public String getFileName() {
        return fileName;
    }

    public String getSerial_tagging() {
        return serial_tagging;
    }

    public void setSerial_tagging(String serial_tagging) {
        this.serial_tagging = serial_tagging;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getMessage_tagging() {
        return message_tagging;
    }

    public void setMessage_tagging(String message_tagging) {
        this.message_tagging = message_tagging;
    }

//    public int getSerial_tagging() {
//        return serial_tagging;
//    }

//    public void setSerial_tagging(int serial_tagging) {
////        this.serial_tagging = serial_tagging;
//    }

    public String getSales_tagging() {
        return sales_tagging;
    }

    public void setSales_tagging(String sales_tagging) {
        this.sales_tagging = sales_tagging;
    }

    public int getFk_tagging() {
        return fk_tagging;
    }

    public void setFk_tagging(int fk_tagging) {
        this.fk_tagging = fk_tagging;
    }
}