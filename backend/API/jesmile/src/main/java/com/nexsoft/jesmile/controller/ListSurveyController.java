package com.nexsoft.jesmile.controller;

import com.nexsoft.jesmile.entity.ListSurvey;
import com.nexsoft.jesmile.repository.ListSurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ListSurveyController {
    @Autowired
    private ListSurveyRepository listSurveyRepository;

    @GetMapping("/listSurvey")
    private List<ListSurvey> getSurvey() {return listSurveyRepository.findAll();}

    @GetMapping("/listSurvey/{salesname}")
    private List<ListSurvey> getListSales(@PathVariable String salesname, LocalDate date) {return listSurveyRepository.getListSurveySales(salesname, LocalDate.now());}

    @PostMapping("/addListSurvey")
    private ListSurvey addListSurvey(@RequestBody ListSurvey listSurvey) {return listSurveyRepository.save(listSurvey);}

    @PostMapping("/addAllListSurvey")
    private List<ListSurvey> addAllListSurvey(@RequestBody List<ListSurvey> listSurveys) {return listSurveyRepository.saveAll(listSurveys);}

    @DeleteMapping("/deleteListSurvey/{id}")
    private String deleteListSurvey(@PathVariable int id) { listSurveyRepository.deleteById(id); return "Data Berhasil dihapus!"; }
}
