package com.nexsoft.jesmile.repository;

import com.nexsoft.jesmile.entity.Tagging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaggingRepository extends JpaRepository<Tagging, Integer> {
    @Query("select t from Tagging t where sales_tagging = ?1")
    List<Tagging> getTaggingBySales(String sales_tagging);
}
