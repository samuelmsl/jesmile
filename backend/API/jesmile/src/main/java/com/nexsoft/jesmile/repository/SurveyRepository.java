package com.nexsoft.jesmile.repository;

import com.nexsoft.jesmile.entity.Survey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SurveyRepository extends JpaRepository<Survey, Integer> {
    @Query("select s from Survey s where s.sales_survey = ?1 ")
    public List<Survey> getSurveyBySales(String sales_survey);
}
